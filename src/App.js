import React, { Component } from 'react';
import  Bg_gitlab  from './assets/images/Bg_gitlab.webp';
import  back  from './assets/images/back.jfif';
import  gitlabcicddiagram  from './assets/images/gitlabcicddiagram.png';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <section>
          <header>
            <h2 className="App-title">CI/CD Gitlab</h2>
          </header>
        </section>
        <section>
          <img src={Bg_gitlab} width="90%" alt="logo"/>
          < h3 className = "App-title" >Este es un nuevo cambio</h3>

          <img style={{
    left: 0,
    right: -2,
    width: '50%',
    zIndex: -1,
          }} src={back} alt="logo"/>
        </section>
        <section>
          <h3 className="App-title">Diagrama de Flujo de trabajao CI/CD Gitlab</h3>
          <img src={gitlabcicddiagram} width="100%" alt="logo" />
        </section>
        <section>
          <h3 className="App-title">Grupo #1 de trabajo</h3>
          <ul className= "App-group-info">
            <li> Jordi Mateo - <span>1085414</span></li>
            <li> JOSE ESCOBOSO - <span>1084756</span></li>
            <li>  JOEL FABIAN - <span>1074586</span></li>
            <li> DAVID PELÁEZ - <span>1086787</span></li>
          </ul>
          < h4 className = "App-title" >Kaking Choi</h4>

        </section>
      </div>
    );
  }
}

export default App;
